package datamerge

import com.typesafe.config.ConfigFactory

// Config Object for the Spark Job
object AppConfig {

  private val config = ConfigFactory.load()

  val hdfsUrl = config.getString("hdfsUrl")

  val baseDir = config.getString("baseDir")

}