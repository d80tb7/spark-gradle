package datamerge.model

import org.apache.spark.sql.{DataFrame, Dataset}

case class Deltas(data: DataFrame, uri: String)

case class Snapshot(data: DataFrame, uri: String, asof: Long)



