package datamerge

import datamerge.dao.Snapshots
import org.apache.spark.sql.SparkSession
import datamerge.dao.Sow


object  DeltasMergingJob {
  def main(args: Array[String]): Unit = {

    implicit val spark = SparkSession.builder.getOrCreate()

    // Load our snapshots
    val snapshots = Snapshots.unprocessedSnapshots
    val deltas = Snapshots.unprocessedDeltas

    Sow.applyDeltas(deltas)

    snapshots.toList.sortBy(_.asof).lastOption match {
      case Some(x) => Sow.applySnapshot(x)
    }

    // Finally mark everything as processed
    Snapshots.markProcessed(snapshots.map(_.uri) ++ deltas.map(_.uri ))
  }

}
