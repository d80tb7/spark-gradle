package datamerge.util

import org.apache.hadoop.fs.RemoteIterator
import scala.collection.AbstractIterator

/**
  * Classes to implicitly convert a Hdfs Remote Iterator into a Scala Iterator
  */
case class RemoteIteratorWrapper[T](underlying: RemoteIterator[T]) extends AbstractIterator[T] with Iterator[T] {
  def hasNext = underlying.hasNext
  def next() = underlying.next()
}

object Conversions {
  implicit def remoteIterToScalaIter[T](underlying: RemoteIterator[T]): Iterator[T] = RemoteIteratorWrapper[T](underlying)
}