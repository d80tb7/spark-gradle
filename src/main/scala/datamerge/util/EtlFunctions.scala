package datamerge.util

import org.apache.spark.sql.{Column, Dataset, SparkSession, _}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

/**
  * Created by chris on 06/01/2017.
  */
object EtlFunctions {

  def getFlattened(df: DataFrame)(implicit spark: SparkSession): DataFrame = {
    import spark.implicits._
    df.groupBy($"objectId", $"propId", $"date")
      .agg(max(struct($"asof", $"value")) as 'tmp)
      .select($"date", $"objectId", $"propId", $"tmp.value")
  }

  def doPivot(df: DataFrame)(implicit spark: SparkSession): DataFrame = {
    import spark.implicits._
    df.groupBy($"objectId", $"date")
      .pivot("propId")
      .agg(first($"value"))
  }

  def fillGaps(df: DataFrame)(implicit spark: SparkSession): DataFrame = {
    import spark.implicits._

    val range = udf((count: Int) => (0 to count).toArray)
    val timestamp_add = (timestamp: Column, days: Column) => timestamp + (days * 86400)
    val date_add = (date: Column, days: Column) => to_date(from_unixtime(timestamp_add(unix_timestamp(date), days)))

    df
      .withColumn("date_to", lead($"date", 1).over(Window.partitionBy(struct($"objectId", $"propId")).orderBy($"date")))
      .withColumn("date_diff", coalesce(datediff($"date_to", $"date") - 1, lit(0)))
      .withColumn("date_offsets", range($"date_diff"))
      .withColumn("date_offset", explode($"date_offsets"))
      .withColumn("date", date_add($"date", $"date_offset"))
      .select($"objectId", $"propId", $"date", $"value")
  }

}
