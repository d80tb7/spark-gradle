package datamerge.dao

import datamerge.model.{Deltas, Snapshot}
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.{Column, SparkSession}
import datamerge.util.Conversions._
import org.apache.spark.sql.catalyst.expressions.Literal
import org.apache.spark.sql.types.DataTypes

object Snapshots extends EtlFilesystem{

  def unprocessedDeltas(implicit spark: SparkSession): Iterable[Deltas] = {
    fs.listFiles(snapshotPartialDir, false).map{ x =>
       val path = x.getPath.toString
       Deltas(spark.read.parquet(path), path)
    }.toIterable
  }

  def unprocessedSnapshots(implicit spark: SparkSession): Iterable[Snapshot] = {
    fs.listFiles(snapshotFullDir, false)
      .map{x =>
        val asOf = asOfFromPath(x.getPath)
        val data = spark.read.parquet(x.toString).withColumn("asof", asofCol(asOf))
        Snapshot(data, x.toString, asOf)
      }.toIterable
  }

  def markProcessed(uris: Iterable[String]) = {
    uris.foreach { x =>
      val from = new Path(x)
      val to = new Path(processedDir, from.getName)
      fs.rename(from, to)
    }
  }

  private def asOfFromPath(file: Path)= {
    val toks = file.getName.split("_")
    toks(1).toLong
  }

  private def asofCol(ts:Long) = new Column(new Literal(ts, DataTypes.LongType))

}
