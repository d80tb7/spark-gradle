package datamerge.dao

import datamerge.AppConfig._
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}


trait EtlFilesystem {

  protected val fs = createFilesystem

  protected val sowDir = new Path(baseDir, "sow")

  protected val snapshotFullDir = new Path(baseDir, "snapshot", "full")

  protected val snapshotPartialDir = new Path(baseDir, "snapshot", "partial")

  protected val processedDir = new Path(baseDir, "processed")


  private def createFilesystem(): FileSystem = {
    val conf = new Configuration()
    conf.set("fs.defaultFS", hdfsUrl)
    FileSystem.get(conf)
  }



}
