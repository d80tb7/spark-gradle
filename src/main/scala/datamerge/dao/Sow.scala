package datamerge.dao

import datamerge.model.{Deltas, Snapshot}
import org.apache.hadoop.fs.Path
import org.apache.spark.sql.SaveMode.{Append, Overwrite}
import org.apache.spark.sql._
import org.apache.spark.sql.functions._


object Sow extends EtlFilesystem {

  protected val sowFile = new Path(sowDir, "sow")

  protected val tmpSowFile = new Path(sowDir, "sow.tmp")

  def getRaw(implicit spark: SparkSession): DataFrame = {
   spark.read.parquet(sowFile.toString)
  }

  def getFlattened(implicit spark: SparkSession): DataFrame = {
    import spark.implicits._
    getRaw.groupBy($"id", $"date")
          .agg(max(struct($"asof", $"value")) as 'tmp)
          .select($"date", $"id", $"tmp.value")
  }

  def applyDeltas(deltas: Iterable[Deltas])(implicit spark: SparkSession) = {
    deltas.foreach(_.data.write.mode(Append).parquet(sowFile.toString))
  }

  def applySnapshot(snapshot: Snapshot)(implicit spark: SparkSession) ={
    import spark.implicits._
    val carryOverEvents = getRaw.filter($"asof" > snapshot.asof)
    carryOverEvents.union(snapshot.data).write.mode(Overwrite).parquet(tmpSowFile.toString)
    fs.rename(tmpSowFile, sowFile)
  }

}
