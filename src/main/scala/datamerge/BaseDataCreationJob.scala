package datamerge

import java.sql.Date

import org.apache.spark.sql.expressions.Window
import datamerge.dao.{Snapshots, Sow}
import datamerge.util.EtlFunctions
import org.apache.spark.sql._
import org.apache.spark.sql.functions._


object  BaseDataCreationJob {

  def main(args: Array[String]): Unit = {

    val obj1 = 1
    val obj2 = 2

    val field1 = 100
    val  field2 = 200

    implicit val spark = SparkSession.builder.getOrCreate()
    import spark.implicits._
    val events = List(
      Event(2, new Date(100, 0, 0), obj1, field1, "chips"),
      Event(1, new Date(100, 0, 0), obj1, field1, "fish"),
      Event(1, new Date(100, 0, 5), obj1, field1, "cod"),
      Event(1, new Date(100, 0, 1), obj1, field2, "salt"),
      Event(1, new Date(100, 0, 5), obj2, field1, "vinegar")
    )
    val df = spark.createDataFrame(events)
    val flattened = EtlFunctions.getFlattened(df)
    val filled = EtlFunctions.fillGaps(flattened)
    EtlFunctions.doPivot(filled).show();
  }

}

case class Event(asOf: Long, date: Date, objectId: Int, propId: Int, value: String);
case class InstrumentData(date: Date, id: Int, Field1: Option[String], Field2: Option[String])
